section .data
msg db  "El coseno  es:",0xA, 0xD
len equ $ -  msg

;prod db  "El producto  es:",0xA, 0xD
;lenp equ $ -  prod



section .bss
    cos resb 10

section .text
   global _start

_start:
   mov rax,4         ;Imprime el mensaje
   mov rbx, 1
   mov rcx, msg
   mov rdx, len
   int 0x80

   mov r8, 3         ;asigna valor a n
   mov rcx, r8

_Loopn:
   mov r8, rcx

   call _signo
;_retsigno:

   call _Exp
;_retExp:

   mov rbx, rax
   pop rax
   imul rax, rbx
   push rax

;   mov rax,4         ;Imprime el mensaje producto
;   mov rbx, 1
;   mov rcx, prod
;   mov rdx, lenp
;   int 0x80



   call _Fact
;_retFact:
	mov rbx, rax
	pop rax
	idiv rbx

	add r9, rax      ; hace la sumatoria y la guarda en r9


mov rcx, [r8]
loop _Loopn
			; imprime el valor del coseno
        add rax, '0'
        mov [cos], rax

        mov rax, 4
        mov rbx, 1
        mov rcx, cos
        mov rdx, 2
        int 0x80

mov rax, 1   ;fin del cálculo
int 0x80

			;Cálculo de suboperaciones

;Calcula el signo
_signo:
 mov rax, -1    ;-1 inicial
; mov rcx, 3   			 ;n
 add rcx, 1
L1:
	imul rax, -1
	loop L1
	push rax
	;call _retsigno
	ret
;Calcula el factorial
_Fact:
   mov rax, 1
;   mov rcx, 3			  ;n
   imul rcx, 2

l2:
	imul rax, rcx
	loop l2
	;call _retFact
	ret

;Calcula el exponente
_Exp:
  mov rbx, 0    ;argumento del coseno
  mov rax, 1    ; valor inicial 1
;  mov rcx, 3   			 ;n
  imul rcx, 2   ;2n
L3:
	imul rax, rbx
	loop L3
	;call _retExp
	ret

