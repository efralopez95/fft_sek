section .data
msg db  "El exponente es:",0xA, 0xD
len equ $ -  msg


section .bss
    exp resb 2

section .text
   global _start

_start:
   mov rax, 4         ;Imprime el mensaje
   mov rbx, 1
   mov rcx, msg
   mov rdx, len
   int 0x80

   mov rbx, 2    ;argumento del coseno
   mov rax, 1    ; valor inicial 1
   mov rcx, 3    ;n
   imul rcx, 2   ;2n


Lexp:
	imul rax, rbx
	loop Lexp

	add rax, '0'
	mov [exp], rax

   	mov rax, 4
   	mov rbx, 1
   	mov rcx, exp
	mov rdx, 2
	int 0x80

mov rax,1             ;system call number (sys_exit)
int 0x80              ;call kernel