section .data
msg db  "El factorial es:",0xA, 0xD
len equ $ -  msg


section .bss
    fact resb 2

section .text
   global _start

_start:
   mov rax, 4         ;Imprime el mensaje
   mov rbx, 1
   mov rcx, msg
   mov rdx, len
   int 0x80

   mov rax, 1
   mov rcx, 2
   imul rcx, 2

l1:
	imul rax, rcx
	loop l1

	add rax, '0'
	mov [fact], rax

   	mov rax, 4
   	mov rbx, 1
   	mov rcx, fact
	mov rdx, 2
	int 0x80

mov rax,1             ;system call number (sys_exit)
int 0x80              ;call kernel