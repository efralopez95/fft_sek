section .data
msg db  "El factorial es:",0xA, 0xD
len equ $ -  msg


section .bss
    signo resb 1

section .text
   global _start

_start:
   mov rax, 4         ;Imprime el mensaje
   mov rbx, 1
   mov rcx, msg
   mov rdx, len
   int 0x80

   mov rax, -1    ;-1 inicial
   mov rcx, 5    ;n
   add rcx, 1


Lsigno:
	imul rax, -1
	loop Lsigno

	add rax, '0'
	mov [signo], rax

   	mov rax, 4
   	mov rbx, 1
   	mov rcx, signo
	mov rdx, 2
	int 0x80

mov rax,1             ;system call number (sys_exit)
int 0x80              ;call kernel