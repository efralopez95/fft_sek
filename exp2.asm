section .data
msg db  "El exponente es:",0xA, 0xD
len equ $ -  msg


section .bss
	digitSpace resb 100
	digitSpacePos resb 8
        exp resb 2

section .text
   global _start

_start:
   mov rax, 4         ;Imprime el mensaje
   mov rbx, 1
   mov rcx, msg
   mov rdx, len
   int 0x80

   mov rbx, 2    ;argumento del coseno
   mov rax, 1    ; valor inicial 1
   mov rcx, 10    ;n
   imul rcx, 3   ;2n


Lexp:
	imul rax, rbx
	loop Lexp

	add rax, '0'
	mov [exp], rax

	mov rax, [exp]
	sub rax, 48
	call _printRAX

	mov rax, 60
	mov rdi, 0
	syscall


_printRAX:
	mov rcx, digitSpace
	mov rbx, 10
	mov [rcx], rbx
	inc rcx
	mov [digitSpacePos], rcx

_printRAXLoop:
	mov rdx, 0
	mov rbx, 10
	div rbx
	push rax
	add rdx, 48

	mov rcx, [digitSpacePos]
	mov [rcx], dl
	inc rcx
	mov [digitSpacePos], rcx

	pop rax
	cmp rax, 0
	jne _printRAXLoop

_printRAXLoop2:
	mov rcx, [digitSpacePos]
	mov rax, 1
	mov rdi, 1
	mov rsi, rcx
	mov rdx, 1
	syscall

	mov rcx, [digitSpacePos]
	dec rcx
	mov [digitSpacePos], rcx

	cmp rcx, digitSpace
	jge _printRAXLoop2
	ret
