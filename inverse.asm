default rel

global main

extern printf
extern _GLOBAL_OFFSET_TABLE_


SECTION .text

main:
        push    rbp
        mov     rbp, rsp
        sub     rsp, 16
        mov     dword [rbp-0CH], 0
        mov     dword [rbp-8H], 3
        mov     dword [rbp-4H], 3
        mov     dword [rbp-10H], 1
        jmp     ?_003

?_001:  mov     eax, dword [rbp-8H]
        sub     eax, dword [rbp-10H]
        mov     edx, dword [rbp-4H]
        mov     ecx, eax
        sar     edx, cl
        mov     eax, edx
        and     eax, 01H
        test    eax, eax
        jz      ?_002
        mov     eax, dword [rbp-10H]
        sub     eax, 1
        mov     edx, 1
        mov     ecx, eax
        shl     edx, cl
        mov     eax, edx
        or      dword [rbp-0CH], eax
?_002:  add     dword [rbp-10H], 1
?_003:  mov     eax, dword [rbp-10H]
        cmp     eax, dword [rbp-8H]
        jle     ?_001
        mov     eax, dword [rbp-0CH]
        mov     esi, eax
        lea     rdi, [rel ?_004]
        mov     eax, 0
        call    printf
        mov     eax, 0
        leave
        ret


SECTION .data


SECTION .bss 


SECTION .rodata

?_004:
        db 25H, 64H, 00H


