section .data
msg db "el logaritmo es: ", 0xA, 0xD
len equ $ - msg

section .bss
	loga resb 2

section .text
	global _start

_start:
	mov rax, 4
	mov rbx, 1
	mov rcx, msg
	mov rdx, len
	int 0x80

	mov    r8, 32
	mov    rax, r8
	mov    r9,rax
	mov    r10,0x0

logaritmo:

	sar    r9,1
	add    r10,0x1
	cmp    r9,0x0
	jne    logaritmo
	mov    rax,r10
	sub    rax,0x1

	add rax, '0'
	mov [loga], rax

	mov rax, 4
	mov rbx, 1
	mov rcx, loga
	mov rdx, 2
	int 0x80

mov rax, 1
int 0x80
